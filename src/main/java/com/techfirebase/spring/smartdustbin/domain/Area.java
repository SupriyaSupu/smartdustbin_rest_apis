package com.techfirebase.spring.smartdustbin.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author SHIVANGI SINGH
 * @since (2018-04-09 16:03:00)
 */
@Entity
public class Area implements Serializable {
  @Id
  @Column(name = "area_id")
  private int areaId;

  @Column(name = "area_name")
  private String areaName;

  @Column(name = "dustbin_id")
  private String dustbinId;

  @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
  @JoinTable(
    name = "Area_Worker",
    joinColumns = {@JoinColumn(name = "Area_Id", referencedColumnName = "area_id")},
    inverseJoinColumns = {@JoinColumn(name = "Worker_Id", referencedColumnName = "worker_id")}
  )
  private List<Worker> workers = new ArrayList<>();

  public Area() {}

  public Area(int areaId, String areaName, String dustbinId, List<Worker> workers) {
    this.areaId = areaId;
    this.areaName = areaName;
    this.dustbinId = dustbinId;
    this.workers = workers;
  }

  public int getAreaId() {
    return areaId;
  }

  public void setAreaId(int areaId) {
    this.areaId = areaId;
  }

  public String getAreaName() {
    return areaName;
  }

  public void setAreaName(String areaName) {
    this.areaName = areaName;
  }

  public String getDustbinId() {
    return dustbinId;
  }

  public void setDustbinId(String dustbinId) {
    this.dustbinId = dustbinId;
  }

  public List<Worker> getWorkers() {
    return workers;
  }

  public void setWorkers(List<Worker> workers) {
    this.workers = workers;
  }
}
