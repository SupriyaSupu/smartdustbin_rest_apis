package com.techfirebase.spring.smartdustbin.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author SHIVANGI SINGH
 * @since (2018-04-09 14:07:38)
 */
@Entity
public class DustbinDetail {
  @Id
  @Column(name = "DustbinDetailId")
  private int dustbinDetailId;

  @Column(name = "DustbinId")
  private String dustbinId;

  @Column(name = "SensorId")
  private String sensorId;

  /*@Column(name = "timestamp")
  private long timestamp;

  @Column(name = "sync_pending")
  private boolean syncPending;*/

  public DustbinDetail() {}

  public DustbinDetail(int dustbinDetailId, String dustbinId, String sensorId) {
    this.dustbinDetailId = dustbinDetailId;
    this.dustbinId = dustbinId;
    this.sensorId = sensorId;
  }

  public int getDustbinDetailId() {
    return dustbinDetailId;
  }

  public void setDustbinDetailId(int dustbinDetailId) {
    this.dustbinDetailId = dustbinDetailId;
  }

  public String getDustbinId() {
    return dustbinId;
  }

  public void setDustbinId(String dustbinId) {
    this.dustbinId = dustbinId;
  }

  public String getSensorId() {
    return sensorId;
  }

  public void setSensorId(String sensorId) {
    this.sensorId = sensorId;
  }
}
